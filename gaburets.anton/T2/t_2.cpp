#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>
#include <set>
#include <algorithm>
#include <numeric>
#include <functional>
#include <iomanip>
#include "Header.hpp"



int main()
{
    std::string input_string = "3 (1;1) (1;3) (3;3)\n3 (1;1) (1;3) (3;3)\n3 (1;1) (1;3) (3;3)\n4 (0;0) (2;1) (2;0) (0;1)\n5 (0;0) (0;1) (1;2) (2;1) (2;0)\n3 (0;0) (-2;0) (0;-2)";
    std::istringstream iss(input_string);

    std::vector<Polygon> polygons;
    while (iss.peek() != EOF)
    {
        Polygon polygon;
        iss >> polygon;
        polygons.push_back(polygon);
    }

    std::string commands = "INTERSECTIONS 3 (2;2) (1;3) (3;3)\nLESSAREA 3 (2;2) (1;3) (3;3)\n";
    std::istringstream com_iss(commands);

    while (com_iss.peek() != EOF)
    {
        std::string command;
        std::getline(com_iss, command);
        size_t semicolon_pos = command.find(' ');
        std::string command2 = command.substr(semicolon_pos + 1);
        command = command.substr(0, semicolon_pos);
        if (command == "AREA") {
            if (command2 == "ODD" || command2 == "EVEN") {
                std::cout << std::setprecision(1) << AREA(command2, polygons) << std::endl;
            }
            else if (command2 == "MEAN") {
                std::cout << std::setprecision(1) << AREA(polygons) << std::endl;
            }
            else if (std::all_of(command2.begin(), command2.end(), ::isdigit)) {
                std::cout << std::setprecision(1) << AREA(std::stoi(command2), polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "MAX") {
            if (command2 == "AREA" || command2 == "VERTEXES") {
                std::cout << std::setprecision(1) << MAX(command2, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "MIN") {
            if (command2 == "AREA" || command2 == "VERTEXES") {
                std::cout << std::setprecision(1) << MIN(command2, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "COUNT") {
            if (command2 == "ODD" || command2 == "EVEN" || std::all_of(command2.begin(), command2.end(), ::isdigit)) {
                std::cout << std::setprecision(1) << COUNT(command2, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "INTERSECTIONS") {
            if (isRightFormat(command2)) {
                Polygon p;
                std::istringstream is(command2);
                is >> p;
                std::cout << INTERSECTIONS(p, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }

        }
        if (command == "LESSAREA") {
            if (isRightFormat(command2)) {
                Polygon p;
                std::istringstream is(command2);
                is >> p;
                std::cout << LESSAREA(p, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }

        }


    }
    return 0;
}
