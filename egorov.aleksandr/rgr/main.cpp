#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include "Dictionary.h"

void printList(std::list<std::string>* input)
{
	std::string result = std::accumulate(input->begin(), input->end(), std::string(), [](const std::string& acc, const std::string& word)
		{
			return acc.empty() ? word : acc + " " + word;
		});
	std::cout << result << std::endl;
}

void printTranslations(std::list<std::string>* translations, const std::string& key) {
	if (translations != nullptr) {
		std::cout << "Переводы для слова " << key << ": ";
		printList(translations);
	}
	else {
		std::cout << "Переводы для слова: " << key << " не найдены" << std::endl;
	}
}

void runFileTests(Dictionary& dict) {
	// Файл должен иметь верную структуру, ошибки не допускаются
	std::ifstream file("file.txt");

	if (!file.is_open()) {
		std::cout << "Файл 'file.txt' не найден" << std::endl;
		return;
	}

	std::string line;
	std::string command;
	std::string key;
	std::string value;

	std::cout << "Начато тестирование из файла..." << std::endl;
	while (std::getline(file, line)) {
		std::istringstream iss(line);
		iss >> command;
		if (command == "INSERT") {
			iss >> key;
			iss >> value;
			dict.insert(key, value);
			std::cout << "Добавлен перевод: " << value << " для слова: " << key << std::endl;
		}
		else if (command == "SEARCH") {
			iss >> key;
			std::list<std::string>* translations = dict.search(key);
			printTranslations(translations, key);
		}
		else if (command == "DELETE") {
			iss >> key;
			std::list<std::string>* translations = dict.search(key);
			if (translations == nullptr)
			{
				std::cout << "Переводы для слова: " << key << " не найдены и не удалены" << std::endl;
				continue;
			}
			dict.deleteKey(key);
			std::cout << "Удален перевод для слова: " << key << std::endl;
		}
		else if (command == "PRINTHEIGHT") {
			std::cout << "Высота дерева: " << dict.getHeight() << std::endl;
		}
	}
	file.close();
}

void printHelpCommands() {
	std::cout << "Выберите команду, введя её номер или название. Доступные команды:" << std::endl;
	std::cout << "1. INSERT <Eng Word> <Translation> - Вставка нового перевода для английского слова" << std::endl;
	std::cout << "2. SEARCH <Eng Word> - Поиск переводов для английского слова" << std::endl;
	std::cout << "3. DELETE <Eng Word> - Удаление переводов для английского слова" << std::endl;
	std::cout << "4. PRINTHEIGHT - Вывод высоты дерева" << std::endl;
	std::cout << "5. RUN_FROM_FILE - Запуск команд из файла file.txt" << std::endl;
	std::cout << "6. SAFETOFILE - Сохранение дерева в файл result.txt" << std::endl;
	std::cout << "7. CLEAR - Очистка консоли" << std::endl;
	std::cout << "8. EXIT - Выход из программы" << std::endl;
}


int main() {
	setlocale(LC_ALL, "Russian");

	Dictionary dict;

	std::string commands = R"(
	INSERT apple яблоко
	INSERT apple сок
	SEARCH apple
	DELETE apple
	SEARCH apple
	INSERT orange апельсин
	SEARCH orange
	PRINTHEIGHT
	RUN_FROM_FILE
	SAFETOFILE
	)";

	std::istringstream iss(commands);

	std::string command;
	printHelpCommands();
	while (iss >> command) {
		std::cout << "> ";

		if (command == "EXIT" || command == "8") {
			break;
		}
		else if (command == "INSERT" || command == "1") {
			std::string key, value;
			iss >> key >> value;
			std::cout << "Введите английское слово: " << key << std::endl;
			std::cout << "Введите перевод: " << value << std::endl;
			dict.insert(key, value);
			std::cout << "Перевод для слова " << key << " добавлен: " << value << std::endl;
		}
		else if (command == "SEARCH" || command == "2") {
			std::string key;
			iss >> key;
			std::cout << "Введите английское слово для которого необходимо найти перевод: " << key << std::endl;
			std::list<std::string>* translations = dict.search(key);
			std::cout << "Результат поиска: " << std::endl;
			printTranslations(translations, key);
		}
		else if (command == "DELETE" || command == "3") {
			std::string key;
			iss >> key;
			std::cout << "Введите ключ который необходимо удалить: " << key << std::endl;

			std::list<std::string>* translations = dict.search(key);
			if (translations == nullptr)
			{
				std::cout << "Переводы для слова: " << key << " не найдены и не удалены" << std::endl;
				continue;
			}
			dict.deleteKey(key);
			std::cout << "Удален перевод для слова: " << key << std::endl;
		}
		else if (command == "PRINTHEIGHT" || command == "4") {
			std::cout << "Высота дерева: " << dict.getHeight() << std::endl;
		}
		else if (command == "RUN_FROM_FILE" || command == "5") {
			runFileTests(dict);
		}
		else if (command == "SAFETOFILE" || command == "6") {
			dict.saveToFile("result.txt");
			std::cout << "Дерево сохранено в файл result.txt" << std::endl;
		}
		else if (command == "CLEAR" || command == "7") {
			std::cout << "\033[2J\033[1;1H"; // Очистка консоли.
			printHelpCommands();
		}
		else {
			std::cout << "Неизвестная команда: " << command << std::endl;
		}
	}

	return 0;
}
