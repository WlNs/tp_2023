#ifndef AVL_TREE_H
#define AVL_TREE_H

#include <string>
#include <list>
#include <numeric>
#include <map>
#include <algorithm>

class AVLTree {
public:
	std::map<std::string, std::list<std::string>> dict;

	std::string toLower(const std::string& str) {
		std::string result = str;
		for (char& c : result) {
			c = std::tolower(static_cast<unsigned char>(c));
		}
		return result;
	}

	int get_height(const std::map<std::string, std::list<std::string>>& map) {
		return get_height_impl(map.cbegin(), map.cend(), map.size());
	}

	void insert(std::string key, std::string value) {
		key = toLower(key);
		value = toLower(value);

		dict[key].push_back(value);
	}

	std::list<std::string>* search(std::string key) {
		key = toLower(key);

		auto it = dict.find(key);

		if (it != dict.end()) {
			return &(it->second);
		}
		else {
			return nullptr;
		}
	}

	void deleteKey(std::string key) {
		key = toLower(key);

		auto it = dict.find(key);

		if (it != dict.end()) {
			dict.erase(it);
		}
	}

	void saveToFile(const std::string& filename) const {
		std::ofstream outFile(filename);
		if (!outFile) {
			std::cerr << "Failed to open the file!\n";
			return;
		}

		for (const auto& pair : dict) {
			outFile << pair.first << ": ";
			std::string result = std::accumulate(pair.second.begin(), pair.second.end(), std::string(),
				[](const std::string& acc, const std::string& word) {
					return acc.empty() ? word : acc + " " + word;
				});
			outFile << result << '\n';
		}

		outFile.close();
	}

private:
	int get_height_impl(typename std::map<std::string, std::list<std::string>>::const_iterator start,
		typename std::map<std::string, std::list<std::string>>::const_iterator end,
		size_t size) const {
		if (size == 0) return 0;
		if (size == 1) return 1;

		auto mid = start;
		std::advance(mid, size / 2);

		return 1 + std::max(get_height_impl(start, mid, size / 2),
			get_height_impl(mid, end, size - size / 2));
	}

};


#endif // AVL_TREE_H