#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iomanip>
#include <cctype>

struct DataStruct
{
	unsigned long long key1;
	unsigned long long key2;
	std::string key3;
};

bool operator<(const DataStruct& a, const DataStruct& b)
{
	if (a.key1 != b.key1)
		return a.key1 < b.key1;
	if (a.key2 != b.key2)
		return a.key2 < b.key2;
	return a.key3.length() < b.key3.length();
}

bool isValidUnsignedLongLong(unsigned long long value) {

	return value >= std::numeric_limits<unsigned long long>::min() && value <= std::numeric_limits<unsigned long long>::max();
}

bool isValidOctal(unsigned long long value) {
	const unsigned long long maxValue = 01777777777777777777777ULL;

	if (value >= std::numeric_limits<unsigned long long>::min() && value <= maxValue) {
		while (value > 0) {
			if (value % 10 >= 8) {
				return false;
			}
			value /= 10;
		}
		return true;
	}
	return false;
}

std::istream& operator>>(std::istream& in, DataStruct& data)
{
	char ch;
	unsigned long long value;
	std::string field_name;

	while (in >> ch && ch != '(');

	if (!in)
		return in;

	int fields_read = 0;
	bool key1_read = false;
	bool key2_read = false;
	bool key3_read = false;

	while (in && fields_read < 3)
	{
		if (in.peek() == ')')
		{
			in.get(ch);
			break;
		}

		in >> ch >> field_name;


		if (field_name == "key1" && !key1_read)
		{
			in >> value;
			if (!isValidUnsignedLongLong(value))
			{
				in.setstate(std::ios::failbit);
				return in;
			}
			data.key1 = value;
			in.ignore(3);
			key1_read = true;
			fields_read++;
		}
		else if (field_name == "key2" && !key2_read)
		{
			in >> value;
			if (!isValidOctal(value))
			{
				in.setstate(std::ios::failbit);
				return in;
			}
			data.key2 = value;
			key2_read = true;
			fields_read++;
		}
		else if (field_name == "key3" && !key3_read)
		{
			in.ignore();
			in >> std::quoted(data.key3);
			key3_read = true;
			fields_read++;
		}
		else
		{
			std::string temp;
			std::getline(in, temp, ':');
		}

		if (in.eof() && fields_read < 3)
		{
			in.setstate(std::ios::failbit);
			return in;
		}
	}

	while (in && ch != ')')
	{
		in >> ch;
	}

	return in;
}


std::ostream& operator<<(std::ostream& out, const DataStruct& data)
{
	out << "(:key1 " << data.key1 << "ull:key2 " << data.key2 << ":key3 \"" << data.key3 << "\":)";

	return out;
}

void test(const std::string& input)
{
	std::cout << "Input data:\n" << input << std::endl;

	std::istringstream iss(input);
	std::vector<DataStruct> data;

	std::copy(std::istream_iterator<DataStruct>(iss),
		std::istream_iterator<DataStruct>(),
		std::back_inserter(data));

	std::sort(data.begin(), data.end());

	std::cout << "Sorted data:\n";
	std::copy(data.begin(), data.end(),
		std::ostream_iterator<DataStruct>(std::cout, "\n"));
	std::cout << std::endl;
}

int main()
{
	test("(:key1 10ull:key2 015:key3 \"Data\":)\n(:key3 \"Data\":key2 014:key1 10ull:)");

	test("(:key1 5ull:key2 014:key3 \"Test\":)\n"
		"(:key1 10ull:key2 015:key3 \"Data\":)\n"
		"(:key1 5ull:key2 010:key3 \"Example\":)");

	test("(:key1 1ull:key2 001:key3 \"A\":)\n"
		"(:key1 1ull:key2 001:key3 \"B\":)\n"
		"(:key1 1ull:key2 001:key3 \"AA\":)");

	test("(:key1 3ull:key2 003:key3 \"Three\":)\n"
		"(:key1 2ull:key2 002:key3 \"Two\":)\n"
		"(:key1 1ull:key2 001:key3 \"One\":)");

	test("(:key1 5ull:key2 015:key3 \"Data\":)\n"
		"(:key2 010:key3 \"Example\":key1 5ull:)\n"
		"(:key3 \"Test\":key1 5ull:key2 014:)");

	test("(:key1 5ull:key2 0159:key3 \"Data\":)\n");

	return 0;
}