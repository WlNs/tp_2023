#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <iomanip>
#include <limits>
#include <algorithm>

struct Point
{
    int x, y;
};

bool operator==(const Point &lhs, const Point &rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

struct Polygon
{
    std::vector<Point> points;
};

std::istream &operator>>(std::istream &is, Point &p)
{
    char c1, c2, c3;
    return is >> c1 >> p.x >> c2 >> p.y >> c3;
}

Polygon parse_polygon(const std::string &line)
{
    Polygon poly;
    std::istringstream iss(line);
    int n;
    iss >> n;
    for (int i = 0; i < n; ++i)
    {
        Point p;
        iss >> p;
        poly.points.push_back(p);
        if (i < n - 1)
        {
            char separator;
            iss >> separator; // считывание разделителя между координатами
        }
    }
    return poly;
}

double area(const Polygon &poly)
{
    double result = 0.0;
    for (size_t i = 0; i < poly.points.size(); ++i)
    {
        size_t j = (i + 1) % poly.points.size();
        result += (poly.points[i].x * poly.points[j].y) - (poly.points[j].x * poly.points[i].y);
    }
    return std::abs(result) / 2.0;
}

int count_polygons(const std::vector<Polygon> &polygons, const std::string &parameter)
{
    int count = 0;
    if (parameter == "EVEN" || parameter == "ODD")
    {
        bool is_even = parameter == "EVEN";
        for (const auto &poly : polygons)
        {
            if (static_cast<int>(poly.points.size()) % 2 == static_cast<int>(is_even))
            {
                count++;
            }
        }
    }
    else
    {
        try
        {
            int num_of_vertexes = std::stoi(parameter);
            for (const auto &poly : polygons)
            {
                if (static_cast<int>(poly.points.size()) == num_of_vertexes)
                {
                    count++;
                }
            }
        }
        catch (const std::invalid_argument &e)
        {
            std::cerr << "Error: invalid parameter for COUNT command.\n";
        }
    }
    return count;
}

bool in_frame(const std::vector<Polygon> &polygons, const Polygon &target_poly)
{
    int min_x = std::numeric_limits<int>::max();
    int max_x = std::numeric_limits<int>::min();
    int min_y = std::numeric_limits<int>::max();
    int max_y = std::numeric_limits<int>::min();

    for (const auto &poly : polygons)
    {
        for (const auto &point : poly.points)
        {
            min_x = std::min(min_x, point.x);
            max_x = std::max(max_x, point.x);
            min_y = std::min(min_y, point.y);
            max_y = std::max(max_y, point.y);
        }
    }

    for (const auto &point : target_poly.points)
    {
        if (point.x < min_x || point.x > max_x || point.y < min_y || point.y > max_y)
        {
            return false;
        }
    }

    return true;
}

int count_intersections(const std::vector<Polygon> &polygons, const Polygon &target_poly);
bool do_lines_intersect(const Point &p1, const Point &p2, const Point &p3, const Point &p4);
bool on_segment(const Point &p1, const Point &p2, const Point &p3);
int count_rectangles(const std::vector<Polygon> &polygons);
bool is_rectangle(const Polygon &poly);
int calculate_angle(const Point &p1, const Point &p2, const Point &p3);
int calculate_direction(const Point &p1, const Point &p2, const Point &p3);

int count_intersections(const std::vector<Polygon> &polygons, const Polygon &target_poly)
{
    int count = 0;
    for (const auto &poly : polygons)
    {
        if (poly.points.size() > 2 && target_poly.points.size() > 2)
        {
            for (size_t i = 0; i < poly.points.size(); ++i)
            {
                size_t j = (i + 1) % poly.points.size();
                for (size_t k = 0; k < target_poly.points.size(); ++k)
                {
                    size_t l = (k + 1) % target_poly.points.size();
                    if (do_lines_intersect(poly.points[i], poly.points[j], target_poly.points[k], target_poly.points[l]))
                    {
                        count++;
                        break;
                    }
                }
            }
        }
    }
    return count;
}

bool do_lines_intersect(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    int d1 = calculate_direction(p3, p4, p1);
    int d2 = calculate_direction(p3, p4, p2);
    int d3 = calculate_direction(p1, p2, p3);
    int d4 = calculate_direction(p1, p2, p4);

    if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) && ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0)))
    {
        return true;
    }
    else if (d1 == 0 && on_segment(p3, p4, p1))
    {
        return true;
    }
    else if (d2 == 0 && on_segment(p3, p4, p2))
    {
        return true;
    }
    else if (d3 == 0 && on_segment(p1, p2, p3))
    {
        return true;
    }
    else if (d4 == 0 && on_segment(p1, p2, p4))
    {
        return true;
    }

    return false;
}

bool on_segment(const Point &p1, const Point &p2, const Point &p3)
{
    if (std::min(p1.x, p2.x) <= p3.x && p3.x <= std::max(p1.x, p2.x) &&
        std::min(p1.y, p2.y) <= p3.y && p3.y <= std::max(p1.y, p2.y))
    {
        return true;
    }
    return false;
}

int count_rectangles(const std::vector<Polygon> &polygons)
{
    int count = 0;
    for (const auto &poly : polygons)
    {
        if (is_rectangle(poly))
        {
            count++;
        }
    }
    return count;
}

bool is_rectangle(const Polygon &poly)
{
    if (poly.points.size() != 4)
    {
        return false;
    }

    std::vector<int> angles;
    for (size_t i = 0; i < poly.points.size(); ++i)
    {
        size_t j = (i + 1) % poly.points.size();
        size_t k = (i + 2) % poly.points.size();
        int angle = calculate_angle(poly.points[i], poly.points[j], poly.points[k]);
        angles.push_back(angle);
    }

    std::sort(angles.begin(), angles.end());

    if (angles[0] == 90 && angles[1] == 90 && angles[2] == 90 && angles[3] == 90)
    {
        return true;
    }
    return false;
}

int calculate_angle(const Point &p1, const Point &p2, const Point &p3)
{
    int dx1 = p2.x - p1.x;
    int dy1 = p2.y - p1.y;
    int dx2 = p3.x - p2.x;
    int dy2 = p3.y - p2.y;

    int dot_product = dx1 * dx2 + dy1 * dy2;
    double length1 = std::sqrt(dx1 * dx1 + dy1 * dy1);
    double length2 = std::sqrt(dx2 * dx2 + dy2 * dy2);

    double cos_theta = dot_product / (length1 * length2);
    double angle = std::acos(cos_theta) * 180.0 / M_PI;

    return static_cast<int>(angle + 0.5); // Round to nearest integer
}

int calculate_direction(const Point &p1, const Point &p2, const Point &p3)
{
    int val = (p2.y - p1.y) * (p3.x - p2.x) - (p2.x - p1.x) * (p3.y - p2.y);
    if (val == 0)
    {
        return 0; // коллинеарны
    }
    return (val > 0) ? 1 : -1; // по часовой или против часовой стрелки
}

int main()
{
    std::string input_filename = "/Users/stanislav/Desktop/c++/tp2/tp2/input.txt";
    std::string commands_filename = "/Users/stanislav/Desktop/c++/tp2/tp2/commands.txt";

    std::ifstream input(input_filename);
    if (!input)
    {
        std::cerr << "Error: unable to open input file.\n";
        return 1;
    }

    std::vector<Polygon> polygons;
    std::string line;
    while (std::getline(input, line))
    {
        if (!line.empty())
        {
            polygons.push_back(parse_polygon(line));
        }
    }
    input.close();

    std::ifstream commands_file(commands_filename);
    if (!commands_file)
    {
        std::cerr << "Error: unable to open commands file.\n";
        return 1;
    }

    while (std::getline(commands_file, line))
    {
        std::istringstream commands(line);
        std::string command;
        commands >> command;

        if (command == "AREA")
        {
            std::string parameter;
            commands >> parameter;
            double total_area = 0.0;
            if (parameter == "EVEN" || parameter == "ODD")
            {
                bool is_even = parameter == "EVEN";
                for (const auto &poly : polygons)
                {
                    if (static_cast<int>(poly.points.size()) % 2 == static_cast<int>(is_even))
                    {
                        total_area += area(poly);
                    }
                }
                std::cout << "AREA " << parameter << ": ";
            }
            else if (parameter == "MEAN")
            {
                if (polygons.empty())
                {
                    std::cout << "No polygons to calculate the mean area.\n";
                    continue;
                }
                else
                {
                    for (const auto &poly : polygons)
                    {
                        total_area += area(poly);
                    }
                    total_area /= polygons.size();
                }
                std::cout << "AREA MEAN: ";
            }
            else
            {
                try
                {
                    int num_of_vertexes = std::stoi(parameter);
                    for (const auto &poly : polygons)
                    {
                        if (static_cast<int>(poly.points.size()) == num_of_vertexes)
                        {
                            total_area += area(poly);
                        }
                    }
                    std::cout << "AREA " << num_of_vertexes << ": ";
                }
                catch (const std::invalid_argument &e)
                {
                    std::cerr << "Error: invalid parameter for AREA command.\n";
                    continue;
                }
            }
            std::cout << std::fixed << std::setprecision(1) << total_area << '\n';
        }
        else if (command == "MAX" || command == "MIN")
        {
            std::string area_command;
            commands >> area_command;
            if (area_command != "AREA")
            {
                std::cerr << "Error: invalid command.\n";
                continue;
            }
            if (polygons.empty())
            {
                std::cout << "No polygons to calculate the " << command << " area.\n";
                continue;
            }
            double result_area = area(polygons[0]);
            for (const auto &poly : polygons)
            {
                double current_area = area(poly);
                if (command == "MAX")
                {
                    if (current_area > result_area)
                    {
                        result_area = current_area;
                    }
                }
                else
                { // command == "MIN"
                    if (current_area < result_area)
                    {
                        result_area = current_area;
                    }
                }
            }
            std::cout << command << " AREA: " << std::fixed << std::setprecision(1) << result_area << '\n';
        }
        else if (command == "COUNT")
        {
            std::string parameter;
            commands >> parameter;
            int count = count_polygons(polygons, parameter);
            std::cout << "COUNT " << parameter << ": " << count << '\n';
        }
        else if (command == "INTERSECTIONS")
        {
            std::string poly_str;
            std::getline(commands, poly_str);
            Polygon target_poly = parse_polygon(poly_str);
            int intersection_count = count_intersections(polygons, target_poly);
            std::cout << "INTERSECTIONS " << target_poly.points.size() << poly_str << ": " << intersection_count << '\n';
        }
        else if (command == "RECTS")
        {
            int rectangle_count = count_rectangles(polygons);
            std::cout << "RECTS: " << rectangle_count << '\n';
        }
        else
        {
            std::cerr << "Error: invalid command.\n";
        }
    }

    commands_file.close();

    return 0;
}

//1. Структуры `Point` и `Polygon` представляют точку и многоугольник соответственно.

//2. Перегруженный оператор `>>` для структуры `Point` позволяет считывать координаты точки из потока ввода.

//3. Функция `parse_polygon` принимает строку и возвращает объект типа `Polygon`, разбирая строку для получения координат многоугольника.

//4. Функция `area` вычисляет площадь многоугольника с использованием формулы площади Гаусса.

//5. Функция `count_polygons` подсчитывает количество многоугольников в заданном векторе `polygons` на основе указанного параметра (четность/нечетность количества вершин или конкретное число вершин).

//6. Функция `in_frame` проверяет, находится ли многоугольник `target_poly` внутри рамки, образованной другими многоугольниками из вектора `polygons`.

//7. Функции `count_intersections`, `do_lines_intersect`, `on_segment`, `count_rectangles`, `is_rectangle`, `calculate_angle` и `calculate_direction` отвечают за подсчет пересечений между многоугольниками, проверку принадлежности точки отрезку, подсчет прямоугольников в векторе многоугольников и вычисление углов и направлений.

//8. Функция `main` является точкой входа в программу. Она открывает файлы `input.txt` и `commands.txt`, считывает данные о многоугольниках из `input.txt`, а затем выполняет команды из `commands.txt`, выводя результаты на экран.

//Команды, которые программа может выполнять, включают вычисление площади многоугольников, поиск максимальной/минимальной площади, подсчет многоугольников по заданному параметру, подсчет пересечений с другим многоугольником и подсчет прямоугольников в векторе многоугольников.
