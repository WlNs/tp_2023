#include "GraphUndirect.h"

GraphUndirect::GraphUndirect(const int& vertices) : numberOfVertices(vertices) {
    if (vertices < 0) {
        throw std::length_error("Invalid number of vertices for graph\n");
        return;
    }
    matrix.resize(numberOfVertices, std::vector<bool>(numberOfVertices, false));
}

GraphUndirect::GraphUndirect() : numberOfVertices(0) {
    matrix.resize(numberOfVertices, std::vector<bool>(numberOfVertices, false));
}

GraphUndirect::GraphUndirect(const GraphUndirect& other) {
    numberOfVertices = other.numberOfVertices;
    matrix = other.matrix;
}

int GraphUndirect::getVertices() {
    return this->numberOfVertices;
}

bool GraphUndirect::addEdge(const int& v1, const int& v2) {
    if ((v1 < 0 || v1 > this->numberOfVertices) || (v2 < 0 || v2 > this->numberOfVertices) || (v1 == v2)) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    if (matrix[v1][v2]) return false;
    matrix[v1][v2] = true;
    matrix[v2][v1] = true;
    return true;
}

bool GraphUndirect::isEdgeInTheGraph(const int& v1, const int& v2) {
    if ((v1 < 0 && v1 >= numberOfVertices) || (v2 < 0 && v2 > numberOfVertices)) {
        throw std::length_error("Vertex is not in range\n");
    }
    return (matrix[v1][v2]);
}

bool GraphUndirect::removeEdge(const int& v1, const int& v2) {
    if ((v1 < 0 || v1 > this->numberOfVertices) || (v2 < 0 || v2 > this->numberOfVertices)) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    if (!matrix[v1][v2]) return false;
    matrix[v1][v2] = false;
    matrix[v2][v1] = false;
    return true;
}

void GraphUndirect::addVertex() {
    int newSize = numberOfVertices + 1;
    std::vector<std::vector<bool>> newMatrix(newSize, std::vector<bool>(newSize, false));

    for (int i = 0; i < numberOfVertices; ++i) {
        for (int j = 0; j < numberOfVertices; ++j) {
            newMatrix[i][j] = matrix[i][j];
        }
    }

    matrix = newMatrix;

    numberOfVertices++;
}

bool GraphUndirect::isVertexInTheGraph(const int& N) {
    return (N < numberOfVertices&& N > -1);
}

bool GraphUndirect::removeVertex(const int& vertex) {
    if (numberOfVertices == 0) {
        throw std::length_error("Graph is empty\n");
    }
    else if (vertex < 0) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    else if (vertex > this->numberOfVertices) return false;
    matrix.erase(matrix.begin() + vertex);
    for (auto& row : matrix) {
        row.erase(row.begin() + vertex);
    }
    numberOfVertices--;
    return true;
}

void GraphUndirect::DFSUtil(const int& vertex, std::vector<bool>& isVisited) {
    isVisited[vertex] = true;
    std::cout << vertex << " ";

    for (int i = 0; i < int(matrix[vertex].size()); ++i) {
        if (matrix[vertex][i] && !isVisited[i])
            DFSUtil(i, isVisited);
    }
}

void GraphUndirect::DFS(const int& startVertex) {
    if (startVertex < 0 || startVertex > this->numberOfVertices) {
        throw std::invalid_argument("Invalid vertex\n");
    }
    std::vector<bool> isVisited(numberOfVertices, false);

    std::cout << "DFS traversal starting from vertex " << startVertex << ": ";
    DFSUtil(startVertex, isVisited);
    std::cout << std::endl;
}

void GraphUndirect::printGraph() {
    std::cout << "   ";
    for (int i = 0; i < numberOfVertices; i++) {
        std::cout << i << "  ";
    }
    std::cout << std::endl;

    for (int i = 0; i < numberOfVertices; i++) {
        std::cout << i << " ";

        for (int j = 0; j < numberOfVertices; j++) {
            if (j >= 10) {
                if (i < 10) {
                    std::cout << std::setw(2) << " ";
                    std::cout << matrix[i][j] << " ";
                }
                else {
                    std::cout << std::setw(0) << ' ';
                    std::cout << matrix[i][j] << "  ";
                }
            }
            if (j < 10) {
                if (i < 10) {
                    std::cout << " ";
                    std::cout << matrix[i][j] << " ";
                }
                else {
                    std::cout << matrix[i][j] << "  ";
                }
            }
        }
        std::cout << std::endl;
    }
}

bool GraphUndirect::isEmpty() {
    return (numberOfVertices == 0);
}

GraphUndirect& GraphUndirect::operator=(GraphUndirect&& other) noexcept {
    if (this != &other) {
        numberOfVertices = other.numberOfVertices;
        matrix = std::move(other.matrix);

    }
    return *this;
}
