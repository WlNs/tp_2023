﻿
#include <iostream>
#include <limits>
#include "GraphDirect.h"
#include "GraphUndirect.h"

void PersonalTest() {
    bool isDirect = 0;
    std::string command;
    int numberOfVertices, source, destination;
    bool flag = false;
    std::cout << "Choose a type of graph to work with: 0 - undirect graph, 1 - direct graph.\n";
    do {
        if (!(std::cin >> isDirect)) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Invalid argument. Try again." << std::endl;
            continue;
        }
        else
            flag = true;
    } while (!flag);
    flag = false;

    if (isDirect) {
        do {
            std::cout << "Set up number of vertices: ";
            if (!(std::cin >> numberOfVertices) || numberOfVertices < 0) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "Invalid argument. Try again." << std::endl;
                continue;
            }
            else
                flag = true;
        } while (!flag);
        flag = false;
        try {
            GraphDirect graphD(numberOfVertices);
            std::cout << "1. To connect two vertices print ADDEDGE and choose two vertices to connect. ADDEDGE 3 5\n";
            std::cout << "2. To add vertex print ADDV. ADDV\n";
            std::cout << "3. To delete vertex print DELV and choose vertex to throw out. DELV 4\n";
            std::cout << "4. To remove arc REMOVEARC and choose two vertices that are connected with this edge. REMOVEARC 1 3\n";
            std::cout << "5. To look at the graph print PRINT. PRINT\n";
            std::cout << "6. To traverse graph using Depth-first search print DFS and choose vertex to start. DFS 5\n";
            std::cout << "7. To check if graph is empty print ISEMPTY. ISEMPTY\n";
            std::cout << "8. To check if there is a vertex in a graph print ISVIN and print it's number. ISVIN 5\n";
            std::cout << "9. To check if there is an arc between two vertices in a graph print ISARCIN and print them. ISARCIN 1 3\n";
            std::cout << "10. To sort vertices via topological sort print TSORT. TSORT\n";
            std::cout << "11. To exit print EXIT. EXIT\n";
            std::cout << "12. Print HELP to get instruction. HELP\n\n";
            while (true) {
                std::cin >> command;
                if (command == "ADDEDGE") {
                    do {
                        if (!(std::cin >> source >> destination)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphD.addArc(source, destination);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "ADDV") {
                    graphD.addVertex();
                }
                else if (command == "ISEMPTY") {
                    if (graphD.isEmpty()) {
                        std::cout << "Graph IS empty.\n";
                    }
                    else {
                        std::cout << "Graph is NOT empty.\n";
                    }
                }
                else if (command == "ISVIN") {
                    do {
                        if (!(std::cin >> source)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    if (graphD.isVertexInTheGraph(source)) {
                        std::cout << "Vertex " << source << " is IN the graph.\n";
                    }
                    else {
                        std::cout << "Vertex " << source << " is NOT in the graph.\n";
                    }
                }
                else if (command == "DELV") {
                    do {
                        if (!(std::cin >> source)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphD.removeVertex(source);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "ISARCIN") {
                    do {
                        if (!(std::cin >> source >> destination)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        if (graphD.isArcInTheGraph(source, destination)) {
                            std::cout << "ARC between " << source << " and " << destination << " exists\n";
                        }
                        else {
                            std::cout << "ARC between " << source << " and " << destination << " DOESN'T exist\n";
                        }
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "REMOVEARC") {
                    do {
                        if (!(std::cin >> source >> destination)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphD.removeArc(source, destination);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "PRINT") {
                    graphD.printGraph();
                }
                else if (command == "DFS") {
                    do {
                        if (!(std::cin >> source)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphD.DFS(source);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }

                else if (command == "TSORT") {
                    if (!graphD.topologicalSort()) {
                        std::cout << "Topological sort is inappropriate for this graph\n";
                    }
                }
                else if (command == "EXIT") {
                    break;
                }
                else if (command == "HELP") {
                    std::cout << "1. To connect two vertices print ADDEDGE and choose two vertices to connect. ADDEDGE 3 5\n";
                    std::cout << "2. To add vertex print ADDV. ADDV\n";
                    std::cout << "3. To delete vertex print DELV and choose vertex to throw out. DELV 4\n";
                    std::cout << "4. To remove arc REMOVEARC and choose two vertices that are connected with this edge. REMOVEARC 1 3\n";
                    std::cout << "5. To look at the graph print PRINT. PRINT\n";
                    std::cout << "6. To traverse graph using Depth-first search print DFS and choose vertex to start. DFS 5\n";
                    std::cout << "7. To check if graph is empty print ISEMPTY. ISEMPTY\n";
                    std::cout << "8. To check if there is a vertex in a graph print ISVIN and print it's number. ISVIN 5\n";
                    std::cout << "9. To check if there is an arc between two vertices in a graph print ISARCIN and print them. ISARCIN 1 3\n";
                    std::cout << "10. To sort vertices via topological sort print TSORT. TSORT\n";
                    std::cout << "11. To exit print EXIT. EXIT\n";
                    std::cout << "12. Print HELP to get instruction. HELP\n\n";
                }
            }
        }
        catch (std::exception& e) {
            std::cerr << e.what();
            return;
        }

    }
    else if (!isDirect) {
        do {
            std::cout << "Set up number of vertices: ";
            if (!(std::cin >> numberOfVertices)) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "Invalid argument. Try again." << std::endl;
                continue;
            }
            else
                flag = true;
        } while (!flag);
        flag = false;
        try {
            GraphUndirect graphUND(numberOfVertices);
            std::cout << "1. To connect two vertices print ADDEDGE and choose two vertices to connect. ADDEDGE 3 5\n";
            std::cout << "2. To add vertex print ADDV. ADDV\n";
            std::cout << "3. To delete vertex print DELV and choose vertex to throw out. DELV 4\n";
            std::cout << "4. To remove arc REMOVEARC and choose two vertices that are connected with this edge. REMOVEARC 1 3\n";
            std::cout << "5. To look at the graph print PRINT. PRINT\n";
            std::cout << "6. To traverse graph using Depth-first search print DFS and choose vertex to start. DFS 5\n";
            std::cout << "7. To check if graph is empty print ISEMPTY. ISEMPTY\n";
            std::cout << "8. To check if there is a vertex in a graph print ISVIN and print it's number. ISVIN 5\n";
            std::cout << "9. To check if there is an arc between two vertices in a graph print ISARCIN and print them. ISARCIN 1 3\n";
            std::cout << "10. To sort vertices via topological sort print TSORT. TSORT\n";
            std::cout << "11. To exit print EXIT. EXIT\n";
            std::cout << "12. Print HELP to get instruction. HELP\n\n";
            while (true) {
                std::cin >> command;
                if (command == "ADDEDGE") {
                    do {
                        if (!(std::cin >> source >> destination)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphUND.addEdge(source, destination);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "ADDV") {
                    graphUND.addVertex();
                }
                else if (command == "ISEMPTY") {
                    if (graphUND.isEmpty()) {
                        std::cout << "Graph IS empty.\n";
                    }
                    else {
                        std::cout << "Graph is NOT empty.\n";
                    }
                }
                else if (command == "ISVIN") {
                    do {
                        if (!(std::cin >> source)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    if (graphUND.isVertexInTheGraph(source)) {
                        std::cout << "Vertex " << source << " is IN the graph.\n";
                    }
                    else {
                        std::cout << "Vertex " << source << " is NOT in the graph.\n";
                    }
                }
                else if (command == "DELV") {
                    do {
                        if (!(std::cin >> source)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphUND.removeVertex(source);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "ISARCIN") {
                    do {
                        if (!(std::cin >> source >> destination)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        if (graphUND.isEdgeInTheGraph(source, destination)) {
                            std::cout << "ARC between " << source << " and " << destination << " exists\n";
                        }
                        else {
                            std::cout << "ARC between " << source << " and " << destination << " DOESN'T exist\n";
                        }
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "REMOVEARC") {
                    do {
                        if (!(std::cin >> source >> destination)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphUND.removeEdge(source, destination);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "PRINT") {
                    graphUND.printGraph();
                }
                else if (command == "DFS") {
                    do {
                        if (!(std::cin >> source)) {
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                            std::cout << "Invalid argument. Try again." << std::endl;
                            continue;
                        }
                        else
                            flag = true;
                    } while (!flag);
                    flag = false;
                    try {
                        graphUND.DFS(source);
                    }
                    catch (std::exception& e) {
                        std::cerr << e.what();
                        continue;
                    }
                }
                else if (command == "TSORT") {
                    std::cout << "Topological sort is inappropriate for undirected graph\n";
                }
                else if (command == "EXIT") {
                    break;
                }
                else if (command == "HELP") {
                    std::cout << "1. To connect two vertices print ADDEDGE and choose two vertices to connect. ADDEDGE 3 5\n";
                    std::cout << "2. To add vertex print ADDV. ADDV\n";
                    std::cout << "3. To delete vertex print DELV and choose vertex to throw out. DELV 4\n";
                    std::cout << "4. To remove arc REMOVEARC and choose two vertices that are connected with this edge. REMOVEARC 1 3\n";
                    std::cout << "5. To look at the graph print PRINT. PRINT\n";
                    std::cout << "6. To traverse graph using Depth-first search print DFS and choose vertex to start. DFS 5\n";
                    std::cout << "7. To check if graph is empty print ISEMPTY. ISEMPTY\n";
                    std::cout << "8. To check if there is a vertex in a graph print ISVIN and print it's number. ISVIN 5\n";
                    std::cout << "9. To check if there is an arc between two vertices in a graph print ISARCIN and print them. ISARCIN 1 3\n";
                    std::cout << "10. To sort vertices via topological sort print TSORT. TSORT\n";
                    std::cout << "11. To exit print EXIT. EXIT\n";
                    std::cout << "12. Print HELP to get instruction. HELP\n\n";
                }
            }
        }
        catch (std::exception& e) {
            std::cerr << e.what();
            return;
        }
    }
}

void myTest1() {

    std::cout << "Creating undirect graph with number of vertices 6\n";
    int V = 6;
    GraphUndirect graph(V);
    graph.printGraph();

    std::cout << "Adding some edges...\n";
    graph.addEdge(5, 1);
    graph.addEdge(5, 2);
    graph.addEdge(0, 1);
    graph.addEdge(1, 3);
    graph.addEdge(2, 4);
    graph.addEdge(2, 1);
    graph.addEdge(4, 1);
    graph.addEdge(5, 4);

    graph.printGraph();

    std::cout << "DFS from 0:\n";

    graph.DFS(0);

    std::cout << "Deleting edge from 1 to 3\n";
    graph.removeEdge(1, 3);
    std::cout << "Removing vertex 2\n";
    graph.removeVertex(2);

    graph.printGraph();
    graph.DFS(0);

    std::cout << "Creating direct graph with number of vertices 7\n";

    GraphDirect graphD(7);

    std::cout << "Adding some edges...\n";
    graphD.addArc(0, 6);
    graphD.addArc(1, 2);
    graphD.addArc(1, 3);
    graphD.addArc(2, 3);
    graphD.addArc(2, 4);
    graphD.addArc(2, 6);
    graphD.addArc(3, 4);
    graphD.addArc(3, 1);


    graphD.printGraph();
    std::cout << "DFS from 0:\n";
    graphD.DFS(0);
    if (!graphD.topologicalSort()) {
        std::cout << "Topological sort is inappropriate for this graph\n";
    }
    std::cout << "Deleting edge from 1 to 3\n";
    graphD.removeArc(1, 3);
    std::cout << "Removing vertex 2\n";
    graphD.removeVertex(2);

    graphD.printGraph();
    std::cout << "DFS from 0:\n";
    graphD.DFS(0);
    if (!graphD.topologicalSort()) {
        std::cout << "Topological sort is inappropriate for this graph\n";
    }
}

void myTest2() {
    srand(unsigned(time(0)));
    std::cout << "Creating undirect graph with number of vertices 10\n";
    int V = 10;
    GraphDirect graph(V);
    std::cout << "Adding random edges\n";
    for (int i = 0; i < 50; i++) {
        int firstNumber = std::rand() % (9 + 1 - 0);
        int secondNumber = std::rand() % (9 + 1 - 0);
        try {
            graph.addArc(firstNumber, secondNumber);
        }
        catch (std::exception&) {
            continue;
        }
    }
    graph.printGraph();
    int DFS = std::rand() % (9 + 1 - 0);
    std::cout << "DFS from " << DFS << ": \n";
    graph.DFS(DFS);
    if (!graph.topologicalSort()) {
        std::cout << "Topological sort is inappropriate for this graph\n";
    }
}

void mainMenu() {
    while (true) {
        std::cout << "Main menu.\n";
        std::cout << "1. Perconal test.\n";
        std::cout << "2. My tests.\n";
        std::cout << "3. To exit.\n";
        bool flag = false;
        int choice = 0;
        do {
            std::cin >> choice;
            if (std::cin.fail() || (choice < 1 || choice > 3)) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "Invalid argument. Try again." << std::endl;
                continue;
            }
            else
                flag = true;
        } while (!flag);
        flag = false;
        if (choice == 1) {
            PersonalTest();
        }
        else if (choice == 2) {
            std::cout << "Choose a test:\n";
            std::cout << "1. Test 1.\n";
            std::cout << "2. Test 2.\n";
            std::cout << "3. To go back.\n";
            do {
                std::cin >> choice;
                if (std::cin.fail() || (choice < 1 || choice > 3)) {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << "Invalid argument. Try again." << std::endl;
                    continue;
                }
                else
                    flag = true;
            } while (!flag);
            flag = false;
            if (choice == 1) {
                myTest1();
            }
            else if (choice == 2) {
                myTest2();
            }
            else {
                continue;
            }
        }
        else {
            break;
        }
    }
}

int main() {

    //mainMenu();
    myTest1();
    myTest2();

    return 0;
}