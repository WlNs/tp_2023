#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>

namespace nspace {
    struct Data {
        long long key1 = 0;
        unsigned long long key2 = 0;
        std::string key3;
    };

    struct DelimiterIO {
        char exp;
    };

    struct StringIO {
        std::string& ref;
    };

    struct SLL_LIT_IO {
        long long& ref;
    };

    struct ULL_BIN_IO {
        unsigned long long& ref;
    };

    struct LabelIO {
        std::string exp;
    };

    struct DigitIO {
        char& ref;
    };

    struct BinDigitIO {
        char& ref;
    };

    struct DataComparator {
        bool operator()(const Data& first, const Data& second);
    };

    class iofmtguard  {
    public:
        iofmtguard(std::basic_ios< char >& s);
        ~iofmtguard();
    private:
        std::basic_ios< char >& s_;
        char fill_;
        std::streamsize precision_;
        std::basic_ios< char >::fmtflags fmt_;
    };

    std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
    std::istream& operator>>(std::istream& in, SLL_LIT_IO&& dest);
    std::istream& operator>>(std::istream& in, ULL_BIN_IO&& dest);
    std::istream& operator>>(std::istream& in, StringIO&& dest);
    std::istream& operator>>(std::istream& in, LabelIO&& dest);
    std::istream& operator>>(std::istream& in, Data& dest);
    std::ostream& operator<<(std::ostream& out, const Data& dest);
    std::istream& operator>>(std::istream& in, DigitIO&& dest);
    std::istream& operator>>(std::istream& in, BinDigitIO&& dest);
}

int main() {
    using nspace::Data;
    std::vector< Data > data;
    std::istringstream iss(
       "(:key1 89ll:key2 0B1100101:key3 \"Data\":) \
        (:key1 10LL:key2 0B10101:key3 \"Data\":) \
        (:key1 89ll : key2 0b1100101 : key3 \"Da\":) \
        (:key 89LL : key2 0b1100101 : key3 \"Da\":) \
        (:key1 89ll:) \
        (:key1 89ll key2 0b1100101 : key3 \"Da\":) \
        (:key3 89ll:key2 0b1100101 : key1 \"Da\":) \
        (:key3 \"Hello\":key2 0b1100101 : key1 4ll:) \
        (:key1 -27ll:key2 0b10101:key3 \"kkk\":)"
    );
    while (!iss.eof()) {
        if (!iss)
        {
            iss.clear();
        }
        std::copy(
            std::istream_iterator< Data >(iss),
            std::istream_iterator< Data >(),
            std::back_inserter(data)
        );
    }
    std::cout << "Data:\n";
    using nspace::DataComparator;
    std::sort(data.begin(), data.end(), DataComparator());
    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Data >(std::cout, "\n")
    );

    return 0;
}

namespace nspace {
    //���������� ��������� ����� ��� �����������
    std::istream& operator>>(std::istream& in, DelimiterIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry) {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && (c != dest.exp)) {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    //���������� ��������� ����� ��� SLL LIT - long long
    std::istream& operator>>(std::istream& in, SLL_LIT_IO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry) {
            return in;
        }
        long long number = 0;
        if (in.peek() != '-') {
            in >> number;
            if (in.peek() == 'l') {
               in >> LabelIO{ "ll" };
            }
            if (in.peek() == 'L') {
                in >> LabelIO{ "LL" };
            }
            if (in)
            {
                dest.ref = number;
            }
        }
        else if (in.peek() == '-') {
           in >> DelimiterIO{ '-' };
           in >> number;
           if (in.peek() == 'l') {
               in >> LabelIO{ "ll" };
           }
           if (in.peek() == 'L') {
               in >> LabelIO{ "LL" };
           }
           dest.ref = (-1) * number;
        }
        else {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    bool isBinary(unsigned long long n) {
        while (n > 0) {
            if (n % 10 != 0 && n % 10 != 1) {
                return false;
            }
            n /= 10;
        }
        return true;
    }

    std::istream& operator>>(std::istream& in, ULL_BIN_IO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry) {
            return in;
        }
        unsigned long long number = 0;
   
        in >> LabelIO{ "0" };
        if (in.peek() == 'B') {
            in >> LabelIO{ "B" };
        }
        else if (in.peek() == 'b') {
            in >> LabelIO{ "b" };
        }
        in >> number;

        if (in && isBinary(number)) {
            dest.ref = number;
        }
        else {
            in.setstate(std::ios::failbit);
        }
  
        return in;
    }

    //���������� ��� ���������� ������
    std::istream& operator>>(std::istream& in, StringIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        return std::getline(in >> DelimiterIO{ '"' }, dest.ref, '"');
    }

    std::istream& operator>>(std::istream& in, LabelIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }

        int length = dest.exp.size();
        char* buffer = new char[length];
        in.read(buffer, length);
        for (int i = 0; i < length; i++) {
            if (buffer[i] != dest.exp[i]) {
                in.setstate(std::ios::failbit);
            }
        } 
        delete[] buffer;
        return in;
    }

    std::istream& operator>>(std::istream& in, DigitIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '\0';
        in >> c;
        if (in && (c < '0' || c > '9'))
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = c;
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, BinDigitIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '\0';
        in >> c;
        if (in && (c != '0' && c != '1'))
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = c;
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, Data& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        Data input;
        using sep = DelimiterIO;
        using label = LabelIO;
        using sll_lit = SLL_LIT_IO;
        using ull_bin = ULL_BIN_IO;
        using str = StringIO;
        using digit = DigitIO;

        in >> sep{ '(' };
        in >> sep{ ':' };
        bool flag1 = false;
        bool flag2 = false;
        bool flag3 = false;
        for (int i = 0; i < 3; i++) {
            in >> label{ "key" };
            char keyNum;
            in >> digit{ keyNum };
                if (keyNum == '1') {
                    in >> sll_lit{ input.key1 };
                    in >> sep{ ':' };
                    flag1 = true;
                }
                else if (keyNum == '2') {
                    in >> ull_bin{ input.key2 };
                    in >> sep{ ':' };
                    flag2 = true;
                }
                else if (keyNum == '3') {
                    in >> str{ input.key3 };
                    in >> sep{ ':' };
                    flag3 = true;
                }
                else {
                    in.setstate(std::ios::failbit);
                }
        }
        in >> sep{ ')' };
        if (in && flag1 && flag2 && flag3) {
            dest = input;
        }
        else {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::ostream& operator<<(std::ostream& out, const Data& src) {
        std::ostream::sentry sentry(out);
        if (!sentry) {
            return out;
        }
        iofmtguard fmtguard(out);
        out << "(:";
        out << "key1 " << std::fixed << std::setprecision(1) << src.key1 << "ll:";
        out << "key2 " << "0b" << src.key2 << ":";
        out << "key3 " << '\"' << src.key3 << '\"';
        out << ":)";
        return out;
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }
    bool DataComparator::operator()(const Data& first, const Data& second) {
        if (first.key1 < second.key1) {
            return true;
        }
        else if (first.key1 > second.key1) {
            return false;
        }
        else if (first.key2 < second.key2) {
            return true;
        }
        else if (first.key2 > second.key2) {
            return false;
        }
        else if (first.key3.size() < second.key3.size()) {
            return true;
        }
        else {
            return false;
        }
    }
}
