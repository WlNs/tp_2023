#include "dictionary.hpp"
#include <iostream>

Dictionary::Dictionary() {}

void Dictionary::insert(const std::string& word, const std::string& translation) {
    words[word] = translation;
}

std::string Dictionary::search(const std::string& word) {
    auto it = words.find(word);
    if (it != words.end()) {
        return it->second;
    }
    else {
        return "";
    }
}

bool Dictionary::remove(const std::string& word) {
    auto it = words.find(word);
    if (it != words.end()) {
        words.erase(it);
        return true;
    }
    else {
        return false;
    }
}

void Dictionary::print() {
    if (words.empty()) {
        std::cout << "The dictionary is empty." << std::endl;
    }
    else {
        for (const auto& pair : words) {
            std::cout << pair.first << " - " << pair.second << std::endl;
        }
    }
}
