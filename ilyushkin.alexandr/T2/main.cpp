﻿#include <iostream>
#include <climits>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <array>
#include <set>
#include <functional>
#include <numeric>
#include <cmath>

namespace nspace
{
    struct Point
    {
        int x, y;
    };

    struct Polygon
    {
        std::vector< Point > points;
    };

    struct Command
    {
        std::string firstCommand;
        std::string secondCommand;
    };

    struct Result
    {
        std::string ref;
    };

    struct DelimiterIO
    {
        char exp;
    };

    struct FirstCommandIO
    {
        std::string& ref;
    };

    struct SecondCommandIO
    {
        std::string& ref;
    };

    struct IntIO
    {
        int& ref;
    };

    class iofmtguard
    {
    public:
        iofmtguard(std::basic_ios< char >& s);
        ~iofmtguard();
    private:
        std::basic_ios< char >& s_;
        char fill_;
        std::streamsize precision_;
        std::basic_ios< char >::fmtflags fmt_;
    };

    bool isEqual(const Point& v1, const Point& v2);
    bool operator==(const Polygon& f1, const Polygon& f2);

    std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
    std::istream& operator>>(std::istream& in, FirstCommandIO&& dest);
    std::istream& operator>>(std::istream& in, SecondCommandIO&& dest);
    std::istream& operator>>(std::istream& in, IntIO&& dest);
    std::istream& operator>>(std::istream& in, Polygon& dest);
    std::istream& operator>>(std::istream& in, Command& dest);
    std::ostream& operator<<(std::ostream& out, const Polygon& dest);
    std::ostream& operator<<(std::ostream& out, const Command& dest);
    std::ostream& operator<<(std::ostream& out, const Result& dest);
    void setInvalidCommand(std::istream& in, Command& dest);

    double tryToFindArea(const Polygon& polygon);
    double findArea(const std::string str, const std::vector<Polygon>& data);
    double findArea(const std::vector<Polygon>& data);
    double findArea(int size, const std::vector<Polygon>& data);
    double findMax(const std::string str, const std::vector<Polygon>& data);
    double findMin(const std::string str, const std::vector<Polygon>& data);
    int count(const std::string str, const std::vector<Polygon>& data);
    int echo(const Polygon& figure, std::vector<Polygon>& data);
    int maxseq(const Polygon& figure, const std::vector<Polygon>& data);

    void getResults(std::vector<Result>& res, std::vector<Polygon>& data, const std::vector<Command>& actions);
}

int main()
{
    using nspace::Polygon;

    std::vector< Polygon > data;
    std::istringstream iss("30 (1;1) (5;3) (3;3)\n3 (1;1) (5;3) (3;3)\n4 (0;0) (0;3) (4;3) (4;0)\n3 (1;1) (5;a) (3;3)\n3 (0;0) (0;2) (4;2)\n3 (0;0) (0;2) (4;2)\n3 (0;0) (-3;0) (-3;-4)\n4 (0;0) (0;-3) (-4;-3) (-4;0)\n3 (0;0) (0;-3) (-4;-3) (-4;0)\n2 (0;0) (0;-3)\n");
    do
    {
        if (!iss)
        {
            iss.clear();
            iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        std::copy
        (
            std::istream_iterator< Polygon >(iss),
            std::istream_iterator< Polygon >(),
            std::back_inserter(data)
        );
    } while (!iss.eof());

    std::cout << "Data:\n";
    std::copy
    (
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Polygon >(std::cout, "\n")
    );

    using nspace::Command;

    std::vector< Command > actions;
    std::istringstream commands("AREA ODD\nAREA EVEN\nAPPlE\nAREA MEAN\nAREA 3d\nAREA COOL\nMAX AREA\nMAX VERTEXES\nMIN AREA\nMIN VERTEXES\nMAX MIN\nCOUNT ODD\nCOUNT EVEN\nCOUNT 3\nCOUNT\nECHO 3 (0;0) (0;2) (4;2)\nECHO 3 (3;3) (1;a) (1;3)\nMAXSEQ 3 (0;0) (0;2) (4;2)\n");
    std::copy
    (
        std::istream_iterator< Command >(commands),
        std::istream_iterator< Command >(),
        std::back_inserter(actions)
    );

    std::cout << "Commands:\n";
    std::copy
    (
        std::begin(actions),
        std::end(actions),
        std::ostream_iterator< Command >(std::cout, "\n")
    );

    using nspace::Result;
    std::vector< Result > res;
    getResults(res, data, actions);
    std::cout << "Results:\n";
    std::copy
    (
        std::begin(res),
        std::end(res),
        std::ostream_iterator< Result >(std::cout, "\n")
    );

    return 0;
}

namespace nspace
{
    std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '\0';
        in >> c;
        if (in && (c != dest.exp))
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, IntIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        in >> dest.ref;
        return in;
    }

    std::istream& operator>>(std::istream& in, Polygon& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        Polygon input;
        Point point{};
        size_t amount = 0;
        {
            in >> std::noskipws >> amount;
            if (!in || amount == UINT_MAX || amount < 3)
            {
                in.setstate(std::ios::failbit);
            }
            using coord = IntIO;
            using sep = DelimiterIO;
            for (size_t i = 0; i < amount; i++)
            {
                in >> std::noskipws >> sep{ ' ' } >> sep{ '(' } >> coord{ point.x } >> sep{ ';' };
                in >> coord{ point.y } >> sep{ ')' };
                if (in)
                {
                    input.points.push_back(point);
                }
            }
        }
        if (in && in.peek() == '\n')
        {
            dest = input;
        }
        else if (in.peek() != '\n')
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, FirstCommandIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        std::set<std::string> valid_strings = { "AREA","MAX","MIN","COUNT","ECHO","MAXSEQ" };
        std::string buffer;
        getline(in, buffer, '\n');
        if (buffer.find(' ') == std::string::npos)
        {
            in.setstate(std::ios::failbit);
            return in;
        }
        size_t i = 0;
        while (i < buffer.length() && buffer[i] != ' ')
        {
            dest.ref += buffer[i];
            i += 1;
        }
        while (i < buffer.length())
        {
            in.unget();
            i++;
        }
        if (!valid_strings.count(dest.ref))
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, SecondCommandIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        getline(in, dest.ref, '\n');
        return in;
    }

    std::istream& operator>>(std::istream& in, Command& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        Command input;
        {
            using strFirst = FirstCommandIO;
            using strSecond = SecondCommandIO;
            in >> std::skipws >> strFirst{ input.firstCommand };
            if (!in)
            {
                setInvalidCommand(in, dest);
                return in;
            }
            in >> std::skipws >> strSecond{ input.secondCommand };
            if (input.firstCommand == "AREA" && input.secondCommand != "EVEN" &&
                input.secondCommand != "ODD" && input.secondCommand != "MEAN")
            {
                if (input.secondCommand == "" || std::find_if_not(input.secondCommand.cbegin(),
                    input.secondCommand.cend(), ::isdigit) != input.secondCommand.cend())
                {
                    setInvalidCommand(in, dest);
                    return in;
                }
            }
            if ((input.firstCommand == "MAX" || input.firstCommand == "MIN") &&
                input.secondCommand != "AREA" && input.secondCommand != "VERTEXES")
            {
                setInvalidCommand(in, dest);
                return in;
            }
            if (input.firstCommand == "COUNT" && input.secondCommand != "EVEN" &&
                input.secondCommand != "ODD")
            {
                if (input.secondCommand == "" || std::find_if_not(input.secondCommand.cbegin(),
                    input.secondCommand.cend(), ::isdigit) != input.secondCommand.cend())
                {
                    setInvalidCommand(in, dest);
                    return in;
                }
            }
            if (input.firstCommand == "ECHO" || input.firstCommand == "MAXSEQ")
            {
                Polygon test;
                std::istringstream iss(input.secondCommand + '\n');
                iss >> test;
                if (!iss)
                {
                    setInvalidCommand(in, dest);
                    return in;
                }
            }
        }
        dest = input;
        return in;
    }

    std::ostream& operator<<(std::ostream& out, const Polygon& src)
    {
        std::ostream::sentry sentry(out);
        if (!sentry)
        {
            return out;
        }
        iofmtguard fmtguard(out);
        out << src.points.size();
        for (size_t i = 0; i < src.points.size(); i++)
        {
            out << " (" << src.points.at(i).x << ';' << src.points.at(i).y << ')';
        }
        return out;
    }

    std::ostream& operator<<(std::ostream& out, const Command& src)
    {
        std::ostream::sentry sentry(out);
        if (!sentry)
        {
            return out;
        }
        iofmtguard fmtguard(out);
        out << src.firstCommand;
        if (src.firstCommand != "<INVALID COMMAND>")
        {
            out << ' ' << src.secondCommand;
        }
        return out;
    }

    std::ostream& operator<<(std::ostream& out, const Result& src)
    {
        std::ostream::sentry sentry(out);
        if (!sentry)
        {
            return out;
        }
        iofmtguard fmtguard(out);
        out << src.ref;
        return out;
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }

    bool isEqual(const Point& v1, const Point& v2)
    {
        return v1.x == v2.x && v1.y == v2.y;
    }

    bool operator==(const Polygon& f1, const Polygon& f2)
    {
        if (f1.points.size() == f2.points.size())
        {
            auto iterator = std::mismatch(f1.points.begin(), f1.points.end(),
                f2.points.begin(), f2.points.end(), isEqual);
            return (iterator.first == f1.points.end());
        }
        else
        {
            return false;
        }
    }

    void setInvalidCommand(std::istream& in, Command& dest)
    {
        dest.firstCommand = "<INVALID COMMAND>";
        dest.secondCommand = "";
        in.clear();
    }

    double tryToFindArea(const Polygon& polygon)
    {
        if (polygon.points.size() < 3)
        {
            return 0.0;
        }
        std::vector<Point>::const_iterator prev = polygon.points.end() - 1;
        std::vector<double> parts(polygon.points.size());
        std::transform(polygon.points.begin(), polygon.points.end(), parts.begin(),
            std::bind([](const Point& v1, const Point& v2)
                { return v1.x * v2.y - v2.x * v1.y; }, std::placeholders::_1, *prev));
        double result = std::accumulate(parts.begin(), parts.end(), 0.0);
        return std::abs(result) / 2.0;
    }

    double findArea(const std::string str, const std::vector<Polygon>& data)
    {
        std::vector<Polygon> figures;
        if (str == "EVEN")
        {
            std::copy_if(data.begin(), data.end(), std::back_inserter(figures), [](const Polygon& polygon)
                { return polygon.points.size() % 2 == 0; });
        }
        if (str == "ODD")
        {
            std::copy_if(data.begin(), data.end(), std::back_inserter(figures), [](const Polygon& polygon)
                { return polygon.points.size() % 2 != 0; });
        }
        std::vector<double> areas(data.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), std::bind(tryToFindArea, std::placeholders::_1));
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double findArea(const std::vector<Polygon>& data)
    {
        if (data.size() < 1)
        {
            return 0.0;
        }
        std::vector<double> areas(data.size());
        std::transform(data.begin(), data.end(), areas.begin(), std::bind(tryToFindArea, std::placeholders::_1));
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result / static_cast<double>(data.size());
    }

    double findArea(int size, const std::vector<Polygon>& data)
    {
        std::vector<Polygon> figures;
        std::copy_if(data.begin(), data.end(), std::back_inserter(figures), [size](const Polygon& polygon)
            { return polygon.points.size() == static_cast<std::vector<Point>::size_type>(size); });
        std::vector<double> areas(data.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), std::bind(tryToFindArea, std::placeholders::_1));
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double findMax(const std::string str, const std::vector<Polygon>& data)
    {
        if (str == "AREA" && data.size() > 0)
        {
            std::vector<double> areas(data.size());
            std::transform(data.begin(), data.end(), areas.begin(), std::bind(tryToFindArea, std::placeholders::_1));
            auto result = *std::max_element(areas.begin(), areas.end());
            return result;
        }
        if (str == "VERTEXES" && data.size() > 0)
        {
            std::vector<int> sizes(data.size());
            std::transform(data.begin(), data.end(), sizes.begin(), [](const Polygon& polygon)
                { return polygon.points.size(); });
            auto result = *std::max_element(sizes.begin(), sizes.end());
            return result;
        }
        return 0;
    }

    double findMin(const std::string str, const std::vector<Polygon>& data)
    {
        if (str == "AREA" && data.size() > 0)
        {
            std::vector<double> areas(data.size());
            std::transform(data.begin(), data.end(), areas.begin(), std::bind(tryToFindArea, std::placeholders::_1));
            auto result = *std::min_element(areas.begin(), areas.end());
            return result;
        }
        if (str == "VERTEXES" && data.size() > 0)
        {
            std::vector<int> sizes(data.size());
            std::transform(data.begin(), data.end(), sizes.begin(), [](const Polygon& polygon)
                { return polygon.points.size(); });
            auto result = *std::min_element(sizes.begin(), sizes.end());
            return result;
        }
        return 0;
    }

    int count(const std::string str, const std::vector<Polygon>& data)
    {
        std::vector<Polygon> sizes;
        if (str == "EVEN")
        {
            std::copy_if(data.begin(), data.end(), std::back_inserter(sizes), [](const Polygon& polygon)
                { return polygon.points.size() % 2 == 0; });
        }
        if (str == "ODD")
        {
            std::copy_if(data.begin(), data.end(), std::back_inserter(sizes), [](const Polygon& polygon)
                { return polygon.points.size() % 2 != 0; });
        }
        if (std::find_if_not(str.cbegin(),
            str.cend(), ::isdigit) == str.cend())
        {
            int count = std::stoi(str);
            std::copy_if(data.begin(), data.end(), std::back_inserter(sizes), [count](const Polygon& polygon)
                { return polygon.points.size() == static_cast<std::vector<Point>::size_type>(count); });
        }
        int size = sizes.size();
        return size;
    }

    int echo(const Polygon& figure, std::vector<Polygon>& data)
    {
        if (data.size() < 1)
        {
            return 0;
        }
        std::vector<Polygon> buffer;
        std::copy_if(data.begin(), data.end(), std::back_inserter(buffer), [&figure](const Polygon& polygon)
            { return polygon == figure; });
        int result = buffer.size();
        std::vector<Polygon> figures;
        std::transform(data.begin(), data.end(), std::back_inserter(figures),
            [figure, &figures](Polygon& object)
            {
                if (object == figure)
                {
                    figures.push_back(object);
                }
                return object;
            });
        data = figures;
        return result;
    }

    int maxseq(const Polygon& figure, const std::vector<Polygon>& data)
    {
        if (data.size() < 1)
        {
            return 0;
        }
        std::vector<size_t> figures(data.size());
        std::vector<size_t>::iterator prevFigure = figures.end() - 1;
        std::transform(figures.begin(), figures.end(), figures.begin(),
            [](size_t& figure) 
            {
                figure = 0;
                return figure; 
            });
        std::transform(data.begin(), data.end(), figures.begin(),
            std::bind([figure](const Polygon& f1, size_t& f2)
                { return (f1 == figure) ? ++f2 : 0; }, std::placeholders::_1, *prevFigure));
        int result = *std::max_element(figures.begin(), figures.end());
        return result;
    }

    void getResults(std::vector<Result>& res, std::vector<Polygon>& data, const std::vector<Command>& actions)
    {
        for (size_t i = 0; i < actions.size(); i++)
        {
            Result result;
            result.ref = actions.at(i).firstCommand + ' ' + actions.at(i).secondCommand + " ANSWER: ";
            if (actions.at(i).firstCommand == "AREA" && (actions.at(i).secondCommand == "EVEN" ||
                actions.at(i).secondCommand == "ODD"))
            {
                result.ref += std::to_string(round(findArea(actions.at(i).secondCommand, data) * 10) / 10);
                result.ref.erase(result.ref.length() - 5);
                res.push_back(result);
            }
            else if (actions.at(i).firstCommand == "AREA" && actions.at(i).secondCommand == "MEAN")
            {
                result.ref += std::to_string(round(findArea(data) * 10) / 10);
                result.ref.erase(result.ref.length() - 5);
                res.push_back(result);
            }
            else if (actions.at(i).firstCommand == "AREA")
            {
                int count = std::stoi(actions.at(i).secondCommand);
                result.ref += std::to_string(round(findArea(count, data) * 10) / 10);
                result.ref.erase(result.ref.length() - 5);
                res.push_back(result);
            }
            else if (actions.at(i).firstCommand == "MAX")
            {
                result.ref += std::to_string(round(findMax(actions.at(i).secondCommand, data) * 10) / 10);
                result.ref.erase(result.ref.length() - 5);
                if (actions.at(i).secondCommand == "VERTEXES")
                {
                    result.ref.erase(result.ref.length() - 2);
                }
                res.push_back(result);
            }
            else if (actions.at(i).firstCommand == "MIN")
            {
                result.ref += std::to_string(round(findMin(actions.at(i).secondCommand, data) * 10) / 10);
                result.ref.erase(result.ref.length() - 5);
                if (actions.at(i).secondCommand == "VERTEXES")
                {
                    result.ref.erase(result.ref.length() - 2);
                }
                res.push_back(result);
            }
            else if (actions.at(i).firstCommand == "COUNT")
            {
                result.ref += std::to_string(count(actions.at(i).secondCommand, data));
                res.push_back(result);
            }
            else if (actions.at(i).firstCommand == "ECHO")
            {
                Polygon test;
                std::istringstream iss(actions.at(i).secondCommand + '\n');
                iss >> test;
                result.ref += std::to_string(echo(test, data));
                std::cout << "Data after command " << actions.at(i).firstCommand
                    << ' ' << actions.at(i).secondCommand << '\n';
                std::copy
                (
                    std::begin(data),
                    std::end(data),
                    std::ostream_iterator< Polygon >(std::cout, "\n")
                );
                res.push_back(result);
            }
            else if (actions.at(i).firstCommand == "MAXSEQ")
            {
                Polygon test;
                std::istringstream iss(actions.at(i).secondCommand + '\n');
                iss >> test;
                result.ref += std::to_string(maxseq(test, data));
                res.push_back(result);
            }
            else
            {
                result.ref += "<INVALID COMMAND>";
                res.push_back(result);
            }
        }
    }
}

