﻿#include "rgr.h"

int main() {
    const char* inputFilename = "input.txt"; // Replace with your input file name

    Myifstream inputFile(inputFilename);
    if (!inputFile) {
        std::cout << "Failed to open the input file." << std::endl;
        return 1;
    }

    unordered_mapWrapper<std::string, vectorWrapper<int>> wordTable;

    std::string word;
    int lineNumber = 1;

    while (!inputFile.eof()) {
        inputFile >> word;

        // Convert word to lowercase for case-insensitive comparison
        std::transform(word.begin(), word.end(), word.begin(), ::tolower);

        // Add word to the wordTable and update line numbers
        wordTable[word].push_back(lineNumber);
        lineNumber++;
    }

    // inputFile will be automatically closed when it goes out of scope

    // Print the contents of wordTable
    for (const auto& entry : wordTable) {
        std::cout << "Word: " << entry.first << std::endl;
        std::cout << "Line Numbers: ";
        for (const auto& lineNumber : entry.second) {
            std::cout << lineNumber << " ";
        }
        std::cout << std::endl;
    }

    return 0;
}
