#ifndef POLYGON_H
#define POLYGON_H
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>
#include <set>
#include <algorithm>
#include <numeric>
#include <functional>
#include <iomanip>

struct Point
{
	int x, y;
};
struct Polygon
{
	std::vector<Point> points;
};
struct ComparisonPoints {
	bool operator()(const Point& point1, const Point& point2) {
		return point1.x == point2.x && point1.y == point2.y;
	}
};
bool operator==(const Polygon& polygon1, const Polygon& polygon2) {
	return polygon1.points.size() == polygon2.points.size() &&
		std::equal(polygon1.points.begin(), polygon1.points.end(), polygon2.points.begin(),ComparisonPoints{});
}
bool isRightFormat(const std::string& str)
{
	std::stringstream ss(str);
	int count;
	ss >> count;

	if (ss.fail() || ss.get() != ' ')
	{
		return false;
	}

	for (int i = 0; i < count; i++)
	{
		std::string pointStr;
		std::getline(ss, pointStr, ' ');

		if (pointStr.empty() || pointStr.front() != '(' || pointStr.back() != ')')
		{
			return false;
		}

		pointStr = pointStr.substr(1, pointStr.length() - 2);
		size_t semicolonPos = pointStr.find(';');

		if (semicolonPos == std::string::npos)
		{
			return false;
		}

		std::string xStr = pointStr.substr(0, semicolonPos);
		std::string yStr = pointStr.substr(semicolonPos + 1);

		if (!std::all_of(xStr.begin(), xStr.end(), ::isdigit) || !std::all_of(yStr.begin(), yStr.end(), ::isdigit))
		{
			return false;
		}
	}

	return ss.eof();
}


std::istream& operator>>(std::istream& is, Point& point)
{
	char openingParenthesis, closingParenthesis, semicolon;
	int x, y;

	// ������ ������� ��������
	is >> openingParenthesis >> x >> semicolon >> y >> closingParenthesis;

	// �������� �� �������� ������ � ������������ �������
	if (is && openingParenthesis == '(' && closingParenthesis == ')' && semicolon == ';')
	{
		point.x = x;
		point.y = y;
	}
	else
	{
		is.setstate(std::ios::failbit); // ��������� ����� ������ ������
	}

	return is;
}


std::istream& operator>>(std::istream& is, Polygon& polygon)
{
	std::string str;
	std::streampos prev_pos = is.tellg();
	std::getline(is, str);
	if (str.empty())
	{
		std::getline(is, str);
	}
	is.seekg(prev_pos);

	if (!isRightFormat(str))
	{
		is.setstate(std::ios::failbit);
		return is;
	}
	int num_points;
	is >> num_points;
	while (num_points)
	{
		Point point;
		is >> point;
		polygon.points.push_back(point);
		num_points--;
	}
	return is;
}

double area(const Polygon& polygon)
{
	if (polygon.points.size() < 3)
	{
		return 0.0;
	}
	std::vector<Point>::const_iterator prev = polygon.points.end() - 1;
	std::vector<double> products(polygon.points.size());
	std::transform(polygon.points.begin(), polygon.points.end(), products.begin(),
		std::bind([](const Point& p1, const Point& p2) { return p1.x * p2.y - p2.x * p1.y; }, std::placeholders::_1, *prev));
	double sum = std::accumulate(products.begin(), products.end(), 0.0);
	return std::abs(sum) / 2.0;
}

double AREA(std::string str, const std::vector<Polygon>& polygons)
{
	double total_area = 0.0;

	if (str == "EVEN")
	{
		for (const Polygon& polygon : polygons)
		{
			if (polygon.points.size() % 2 == 0)
			{
				total_area += area(polygon);
			}
		}
	}
	else if (str == "ODD")
	{
		for (const Polygon& polygon : polygons)
		{
			if (polygon.points.size() % 2 != 0)
			{
				total_area += area(polygon);
			}
		}
	}

	return total_area;
}


double AREA(const std::vector<Polygon>& polygons)
{
	if (polygons.empty())
	{
		return 0.0;
	}

	double total_area = std::accumulate(polygons.begin(), polygons.end(), 0.0,
		[](double acc, const Polygon& p) {
			return acc + area(p);
		});

	return total_area / polygons.size();
}


double AREA(std::size_t size, const std::vector<Polygon>& polygons)
{
	std::vector<Polygon> Shapes;
	std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(Shapes), [size](const Polygon& polygon)
		{return polygon.points.size() == size; });

	double total_area = std::accumulate(Shapes.begin(), Shapes.end(), 0.0,
		[](double acc, const Polygon& shape) {
			return acc + area(shape);
		});

	return total_area;
}

double MAX(std::string str, const std::vector<Polygon>& polygons)
{
	if (str == "AREA" && !polygons.empty())
	{
		std::vector<double> areas;
		areas.reserve(polygons.size());
		for (const auto& polygon : polygons)
		{
			areas.push_back(area(polygon));
		}
		auto max_area = *std::max_element(areas.begin(), areas.end());
		return max_area;
	}
	if (str == "VERTEXES" && !polygons.empty())
	{
		std::vector<int> sizes;
		sizes.reserve(polygons.size());
		for (const auto& polygon : polygons)
		{
			sizes.push_back(polygon.points.size());
		}
		auto max_size = *std::max_element(sizes.begin(), sizes.end());
		return max_size;
	}
	return 0;
}


double MIN(std::string str, const std::vector<Polygon>& polygons)
{
	if (str == "AREA" && !polygons.empty())
	{
		std::vector<double> areas;
		areas.reserve(polygons.size());
		for (const auto& polygon : polygons)
		{
			areas.push_back(area(polygon));
		}
		auto min_area = *std::min_element(areas.begin(), areas.end());
		return min_area;
	}
	if (str == "VERTEXES" && !polygons.empty())
	{
		std::vector<int> sizes;
		sizes.reserve(polygons.size());
		for (const auto& polygon : polygons)
		{
			sizes.push_back(polygon.points.size());
		}
		auto min_size = *std::min_element(sizes.begin(), sizes.end());
		return min_size;
	}
	return 0;
}


int COUNT(std::string str, const std::vector<Polygon>& polygons)
{
	if (str == "EVEN")
	{
		auto count = std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon) {
			return polygon.points.size() % 2 == 0;
			});
		return count;
	}
	if (str == "ODD")
	{
		auto count = std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon) {
			return polygon.points.size() % 2 != 0;
			});
		return count;
	}
	if (std::all_of(str.begin(), str.end(), ::isdigit))
	{
		int targetSize = std::stoi(str);
		auto count = std::count_if(polygons.begin(), polygons.end(), [targetSize](const Polygon& polygon) {
			return polygon.points.size() == static_cast<std::vector<Point>::size_type>(targetSize);

			});
		return count;
	}
	return 0;
}

int ECHO(const Polygon& polygon, std::vector<Polygon>& polygons)
{
	int count = 0;
	auto it = polygons.begin();
	while (it != polygons.end())
	{
		if (*it == polygon)
		{
			it = polygons.insert(it + 1, polygon);
			count++;
		}
		++it;
	}
	return count;
}

int RIGHTSHAPES(const std::vector<Polygon>& polygons)
{
	int count = 0;
	for (const auto& polygon : polygons)
	{
		bool hasRightShape = std::any_of(polygon.points.begin(), polygon.points.end(), [&](const Point& point) {
			const auto& next = polygon.points.back();
		return point.x == next.x || point.y == next.y;
			});

		if (hasRightShape)
			count++;
	}

	return count;
}

std::string PRINTENTER(const std::vector<Polygon>& polygons)
{
	std::ostringstream oss;
	for (size_t i = 0; i < polygons.size(); i++)
	{
		oss << polygons[i].points.size();
		for (const auto& point : polygons[i].points)
		{
			oss << " (" << point.x << ";" << point.y << ")";
		}
		if (i != polygons.size() - 1)
		{
			oss << "\n";
		}
	}
	std::string result = oss.str();
	if (!result.empty() && result[result.size() - 1] == '0')
	{
		result.erase(result.size() - 1);
	}
	return result;
}

#endif
