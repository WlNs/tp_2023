#include <iostream>
#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>

int main()
{
	//-----���������� ����������� ����� ���������------------------------------------------------------------------------

	// ��� �������� ������������ �������� std::copy(), ������� ��������� ��� ���������:
	// ������ ��� - ��������� ������ � ����� ����������� ���������,
	// ������ - ��������, ����������� ���� ��������� �����������

	// ����� ������� � �����
	std::vector<int> v{ 1, 2, 3 };
	std::ostream_iterator<int> out(std::cout, ", ");
	std::copy(v.begin(), v.end(), out);

	std::cout << '\n';

	// ���� ������� �� ������ istringstream
	std::vector<int> v2{ 4, 5, 6, 7 };
	std::istringstream iss("8 9 10");
	std::istream_iterator<int> beg(iss);
	std::istream_iterator<int> end;
	std::copy(beg, end, v2.begin());
	std::copy(v2.begin(), v2.end(), out);

	std::cout << '\n';

	// ����������� ������� � ������ ������
	std::vector<int> v3{ 4, 5, 6, 7 };
	std::copy(v.begin(), v.end(), v3.begin());
	std::copy(v3.begin(), v3.end(), out);

	std::cout << '\n';

	// ������� ��������� � ������ � ������� back_insert_iterator<>
	std::vector<int> v4{ 4, 5, 6, 7 };
	std::copy(v.begin(), v.end(), std::back_inserter(v4));
	std::copy(v4.begin(), v4.end(), out);

	std::cout << '\n';
	return 0;
}